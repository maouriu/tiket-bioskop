<!DOCTYPE html>
<html>
<head lang="en" dir="ltr">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>LOGIN</title>
    <link rel="stylesheet" href="style_login.css">
</head>
<body>
    <div class="wrapper">
    <div class="title">
        Login From
    </div>
    <form method="POST" action="index.php">

        <input type="hidden" name="tujuan" value="LOGIN">
        <div class="field">
            <input name="email" type="email" required>
            <label>Masukan Email</label>
        </div>
        <div class="field">
            <input name="password" type="password" required>
            <label>Masukan Password</label>
        </div>
        <div class="content">
            <div class="checkbox">
                <input type="checkbox" id="ingatkan-saya">
                <label for="inagtkan-saya">Ingatkan saya</label>
            </div>
            <div class="pass-link">
                <a href="#">Lupa Password?</a>
            </div>
        </div>
        <div class="field">
            <input href="Index.html" type="submit" value="login">
        </div>
        <div class="signup-link">
            <p>Belum menjadi anggota?</p>
            <a href="register.php">Daftar Sekarang</a>
        </div>
    </from>
    </div>
</body>
</html>